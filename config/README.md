# CONFIG

## Live template

The file named 'template_LIVE.sh' is a template configuration file.  It uses 
simple name/value pairs.  I may eventually redo this all in ansible, 
but it just isn't worth it now.  It should be copied to the config/live/ directory 
and changed to LIVE.sh.   Any live configuration changes will be done here.
WARNING: it will not be pushed to git so keep it safe.

