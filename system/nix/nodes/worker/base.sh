#!/usr/bin/env bash

# Setup
SCRIPT_DIR=$( dirname -- "$( readlink -f -- "$0"; )"; )
source ../../../../../config/STATIC.sh

# Install packages
apt-get update && \
    apt-get install -y vim

# Install Docker
install -m 0755 -d /etc/apt/keyrings && \
    apt-get install -y curl gpg && \
    curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg && \
    chmod a+r /etc/apt/keyrings/docker.gpg && \
    echo   "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
        "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null  && \
    apt-get update && \
    apt-get -y install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
echo "Docker installed"

# Install Docker CRI
cd /tmp
wget https://github.com/Mirantis/cri-dockerd/releases/download/v${GI_CRI_VERSION}/cri-dockerd-${GI_CRI_VERSION}.amd64.tgz && \
    tar xzf cri-dockerd-${GI_CRI_VERSION}.amd64.tgz && \
    cd cri-dockerd && \
    mkdir -p /usr/local/bin && \
    install -o root -g root -m 0755 cri-dockerd /usr/local/bin/cri-dockerd && \
    wget https://raw.githubusercontent.com/Mirantis/cri-dockerd/master/packaging/systemd/cri-docker.service && \
    wget https://raw.githubusercontent.com/Mirantis/cri-dockerd/master/packaging/systemd/cri-docker.socket && \
    mv cri-docker.socket cri-docker.service /etc/systemd/system/ && \
    sed -i -e 's,/usr/bin/cri-dockerd,/usr/local/bin/cri-dockerd,' /etc/systemd/system/cri-docker.service && \
    systemctl daemon-reload  && \
    systemctl enable cri-docker.service && \
    systemctl enable --now cri-docker.socket

    cd /tmp
    rm -rf cri-dockerd-${GI_CRI_VERSION}.amd64.tgz cri-dockerd

    if systemctl status cri-docker.socket | grep 'active (listening)' ; then
        echo "CRI running."
    else
        echo "ERROR: CRI not running.  Giving up." ;
        exit 9
    fi

# Install Kubernetes bits
apt-get update && \
    apt-get install -y apt-transport-https ca-certificates curl && \
    curl -fsSL https://packages.cloud.google.com/apt/doc/apt-key.gpg | gpg --dearmor -o /etc/apt/keyrings/kubernetes-archive-keyring.gpg && \
    echo "deb [signed-by=/etc/apt/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | tee /etc/apt/sources.list.d/kubernetes.list && \
    apt-get update && \
    apt-get install -y kubelet kubeadm kubectl && \
    apt-mark hold kubelet kubeadm kubectl

echo "Done base"
