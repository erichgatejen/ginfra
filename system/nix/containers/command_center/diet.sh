#!/usr/bin/env bash

#!/bin/sh

rm -rf /usr/share/doc/
rm -rf /usr/doc
rm -rf /usr/man
rm -rf /usr/share/X11
rm -rf /usr/share/icons/*

rm -rf /usr/lib/x86_64-linux-gnu/perl

rm -rf /usr/lib/llvm-9/share/man

rm -rf /usr/share/perl
rm -rf /usr/lib/x86_64-linux-gnu/perl

rm -rf /usr/share/clang/scan-build-py-9/tests

rm -rf /usr/share/applications/*
rm -rf /usr/share/perl5
rm -rf /usr/share/emacs
rm -rf /usr/share/doc-base

